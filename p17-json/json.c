#include <stdio.h>
#include <jansson.h>

const char * json_text = "{\"lectiveSemesters\":[{\"lectiveSemesterId\":1,\"shortName\":\"0910i\",\"startYear\":2009,\"term\":1,\"termName\":\"Fall\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/1\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":2,\"shortName\":\"0910v\",\"startYear\":2009,\"term\":2,\"termName\":\"Spring\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/2\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":3,\"shortName\":\"1011i\",\"startYear\":2010,\"term\":1,\"termName\":\"Fall\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/3\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":4,\"shortName\":\"1011v\",\"startYear\":2010,\"term\":2,\"termName\":\"Spring\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/4\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":5,\"shortName\":\"1112i\",\"startYear\":2011,\"term\":1,\"termName\":\"Fall\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/5\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":6,\"shortName\":\"1112v\",\"startYear\":2011,\"term\":2,\"termName\":\"Spring\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/6\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":7,\"shortName\":\"1213i\",\"startYear\":2012,\"term\":1,\"termName\":\"Fall\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/7\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":8,\"shortName\":\"1213v\",\"startYear\":2012,\"term\":2,\"termName\":\"Spring\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/8\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":9,\"shortName\":\"1314i\",\"startYear\":2013,\"term\":1,\"termName\":\"Fall\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/9\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}},{\"lectiveSemesterId\":10,\"shortName\":\"1314v\",\"startYear\":2013,\"term\":2,\"termName\":\"Spring\",\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters/10\",\"root\":\"http://thoth.cc.e.ipl.pt/api/v1\"}}],\"_links\":{\"self\":\"http://thoth.cc.e.ipl.pt/api/v1/lectivesemesters\"}}";

int main() {

   json_error_t error;
   json_t * thoth_terms = json_loads(json_text, 0, &error);

   if (!thoth_terms) {
      fprintf(stderr, "%s\n", error.text);
      return 1;
   }

   json_t * terms = json_object_get(thoth_terms, "lectiveSemesters");
   size_t nterms = json_array_size(terms);
   
   printf("Numbers of terms: %lu\n", nterms);

   for (int i = 0; i < nterms; ++i) {
      json_t * term = json_array_get(terms, i);
      json_int_t year = json_integer_value(json_object_get(term, "startYear"));
      json_int_t nt   = json_integer_value(json_object_get(term, "term"));
      printf("%" JSON_INTEGER_FORMAT "/%" JSON_INTEGER_FORMAT " - %s\n", year, year + 1, nt == 1 ? "Fall" : "Spring");
   } 

   int idx;
   json_t * trm;
   json_array_foreach(terms, idx, trm) {
      json_int_t year = json_integer_value(json_object_get(trm, "startYear"));
      json_int_t nt   = json_integer_value(json_object_get(trm, "term"));
      printf("%" JSON_INTEGER_FORMAT "/%" JSON_INTEGER_FORMAT " - %s\n", year, year + 1, nt == 1 ? "Fall" : "Spring");
   }

   json_decref(thoth_terms);

   return 0;
}
