#include <stdlib.h>
#include <stdio.h>

#include "dlist.h"

dlist_t myList;

dnode_t node1;

int main() {
	
	dnode_t node2;
	
	dnode_t * pnode3 = (dnode_t *)malloc(sizeof (dnode_t));
	
	printf("&node1 : %p\n", (void*)&node1);
	printf("&node2 : %p\n", (void*)&node2);
	printf("pnode3 : %p\n", (void*)pnode3);
	
	dlist_init(&myList);
	
	dlist_insertFirst(&myList, &node1);
	dlist_insertAfter(&node1, &node2);
	dlist_insertLast(&myList, pnode3);
	
	dnode_t * pnode;
	for (pnode = myList.next; pnode != &myList; pnode = pnode->next) {
		printf("%p ", (void*)pnode);
	}
	putchar('\n');
	
	free(pnode3);
	
	return 0;
}
