#include "Point.h"
#include "Point3D.h"

#include <iostream>
#include <stdio.h>

using namespace std;

void useThePoint(Point * p) {
	cout << p->getMagnitude() << '\n';
	p->printTo(stdout); putchar('\n');
}

Point p0(1, 2);

int main() {
	Point p1(3, 4);
	Point * pp2 = new Point(5, 7);
	
	Point3D p3(3, 4, 5);
	Point3D * pp4 = new Point3D(15, 17, 19);
	
	Point * pp5 = &p3;
	
	cout << "sizeof (Point)   : " << sizeof (Point) << '\n';
	cout << "sizeof (Point3D) : " << sizeof (Point3D) << '\n';

	double mag0 = p0.getMagnitude();	cout << mag0 << '\n';
	double mag1 = p1.getMagnitude();	cout << mag1 << '\n';
	double mag2 = pp2->getMagnitude();	cout << mag2 << '\n';
	double mag3 = p3.getMagnitude();	cout << mag3 << '\n';
	double mag4 = pp4->getMagnitude();	cout << mag4 << '\n';
	double mag5 = pp5->getMagnitude();	cout << mag5 << '\n';
	
	p0.printTo(stdout);		putchar('\n');
	p1.printTo(stdout);		putchar('\n');
	pp2->printTo(stdout);	putchar('\n');
	p3.printTo(stdout);		putchar('\n');
	pp4->printTo(stdout);	putchar('\n');
	pp5->printTo(stdout);	putchar('\n');
	
	useThePoint(&p1);
	useThePoint(&p3);
	
	delete pp4;
	delete pp2;
	return 0;
}
