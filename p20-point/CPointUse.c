#include "CPoint.h"
#include "CPoint3D.h"

#include <stdlib.h>
#include <stdio.h>

void useThePoint(Point * p) {
	printf("%f\n", p->vptr->getMagnitude(p));
	p->vptr->printTo(p, stdout); putchar('\n');
}

Point p0;

int main() {
	Point p1;
	Point * pp2;
	Point3D p3;
	Point3D * pp4;
	Point * pp5;
	
	Point_ctor(&p0, 1, 2);
	Point_ctor(&p1, 3, 4);
	pp2 = (Point *)malloc(sizeof (Point));
	Point_ctor(pp2, 5, 7);
	Point3D_ctor(&p3, 3, 4, 5);
	pp4 = (Point3D *)malloc(sizeof (Point3D));
	Point3D_ctor(pp4, 15, 17, 19);
	pp5 = (Point*)&p3;

	printf("sizeof (Point)   : %u\n", sizeof (Point));
	printf("sizeof (Point3D) : %u\n", sizeof (Point3D));

	double mag0 = Point_getMagnitude(&p0); printf("%f\n", mag0);
	double mag1 = Point_getMagnitude(&p1); printf("%f\n", mag1);
	double mag2 = Point_getMagnitude(pp2); printf("%f\n", mag2);
	double mag3 = Point3D_getMagnitude(&p3); printf("%f\n", mag3);
	double mag4 = Point3D_getMagnitude(pp4); printf("%f\n", mag4);
	double mag5 = Point_getMagnitude(pp5); printf("%f\n", mag5);
	
	Point_printTo(&p0, stdout);	putchar('\n');
	Point_printTo(&p1, stdout);	putchar('\n');
	Point_printTo(pp2, stdout);	putchar('\n');
	Point3D_printTo(&p3, stdout);	putchar('\n');
	Point3D_printTo(pp4, stdout);	putchar('\n');
	Point_printTo(pp5, stdout);	putchar('\n');
	
	useThePoint(&p1);
	useThePoint((Point*)&p3);
	
	free(pp4);
	free(pp2);
	return 0;
}
