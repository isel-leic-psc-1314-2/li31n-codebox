#ifndef CPOINT_H
#define CPOINT_H

#include <stdio.h>

typedef struct pointMethods PointMethods;
typedef struct point Point;

struct pointMethods {
	double (*getMagnitude)(Point * this);
	void   (*printTo)(Point * this, FILE * out);
};

struct point {
	const PointMethods * vptr;
	int x;
	int y;
};

void   Point_ctor(Point * this, int x, int y);
double Point_getMagnitude(Point * this);
void   Point_translate(Point * this, int dx, int dy);
void   Point_printTo(Point * this, FILE * out);

#endif
