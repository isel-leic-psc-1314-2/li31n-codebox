#ifndef POINT_H
#define POINT_H

#include <stdio.h>

class Point {
protected:
	int x;
	int y;
public:
	Point(int x, int y);
	virtual double getMagnitude();
	void translate(int dx, int dy);
	virtual void printTo(FILE * out);
};

#endif
