#include "CPoint.h"

#include <stdio.h>
#include <math.h>

static const PointMethods Point_vtable = {
	.getMagnitude = Point_getMagnitude,
	.printTo      = Point_printTo
};

void Point_ctor(Point * this, int x, int y) {
	this->vptr = &Point_vtable;
	this->x = x;
	(*this).y = y;
}
	
double Point_getMagnitude(Point * this) {
	return sqrt(this->x * this->x + this->y * this->y);
}
	
void Point_translate(Point * this, int dx, int dy) {
	this->x += dx;
	this->y += dy;
}
	
void Point_printTo(Point * this, FILE * out) {
	fprintf(out, "{ x: %d, y: %d }", this->x, this->y);
}
