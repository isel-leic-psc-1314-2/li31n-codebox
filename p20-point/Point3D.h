#ifndef POINT3D_H
#define POINT3D_H

#include "Point.h"

class Point3D : public Point {
private:
	int z;
public:
	Point3D(int x, int y, int z);
	double getMagnitude();
	void translate(int x, int y, int z);
	void printTo(FILE * out);
};

#endif
