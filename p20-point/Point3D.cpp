#include "Point3D.h"

#include <stdio.h>
#include <math.h>

Point3D::Point3D(int x, int y, int z) : Point(x, y) {
	this->z = z;
}

double Point3D::getMagnitude() {
	return sqrt(x*x + y*y + z*z);
}

void Point3D::translate(int dx, int dy, int dz) {
	Point::translate(dx, dy);
	z += dz;
}

void Point3D::printTo(FILE * out) {
	fprintf(out, "{ x: %d, y: %d, z: %d }", x, y, z);
}
