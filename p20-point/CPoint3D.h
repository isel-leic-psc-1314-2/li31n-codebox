#ifndef CPOINT3D_H
#define CPOINT3D_H

#include "CPoint.h"

typedef struct point3d {
	Point super;
	int   z;
} Point3D;

void   Point3D_ctor(Point3D * this, int x, int y, int z);
double Point3D_getMagnitude(Point3D * this);
void   Point3D_translate(Point3D * this, int x, int y, int z);
void   Point3D_printTo(Point3D * this, FILE * out);

#endif
