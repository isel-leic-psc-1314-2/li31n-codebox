#include "CPoint3D.h"

#include <stdio.h>
#include <math.h>

static const PointMethods Point3D_vtable = {
	.getMagnitude = (double (*)(Point *))         Point3D_getMagnitude,
	.printTo      = (void   (*)(Point *, FILE *)) Point3D_printTo
};

void Point3D_ctor(Point3D * this, int x, int y, int z) {
	Point_ctor((Point *)this, x, y);
	this->super.vptr = &Point3D_vtable;
	this->z = z;
}

double Point3D_getMagnitude(Point3D * this) {
	return sqrt(this->super.x*this->super.x +
	            this->super.y*this->super.y +
	            this->z*this->z);
}

void Point3D_translate(Point3D * this, int dx, int dy, int dz) {
	Point_translate((Point *)this, dx, dy);
	this->z += dz;
}

void Point3D_printTo(Point3D * this, FILE * out) {
	fprintf(out, "{ x: %d, y: %d, z: %d }", this->super.x, this->super.y, this->z);
}
