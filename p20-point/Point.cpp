#include "Point.h"

#include <stdio.h>
#include <math.h>

Point::Point(int x, int y) {
	this->x = x;
	(*this).y = y;
}

double Point::getMagnitude() {
	return sqrt(x*x + y*y);
}

void Point::translate(int dx, int dy) {
	x += dx;
	y += dy;
}

void Point::printTo(FILE * out) {
	fprintf(out, "{ x: %d, y: %d }", x, y);
}
