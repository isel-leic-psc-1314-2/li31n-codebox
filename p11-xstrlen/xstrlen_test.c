#include <stdio.h>

unsigned xstrlen(const char * str);

const char * str1 = "ISEL";
const char str2[] = "PSC";

int main() {
	
	unsigned len1 = xstrlen(str1);
	unsigned len2 = xstrlen(str2);
	unsigned len3 = xstrlen("LI31N");
	
	printf("xstrlen(\"ISEL\")  == %u\n", len1);
	printf("xstrlen(\"PSC\")   == %u\n", len2);
	printf("xstrlen(\"LI31N\") == %u\n", len3);
	
	return 0;
}
