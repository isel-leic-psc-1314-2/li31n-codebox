
	.text
	
	.globl find_any
find_any:
	pushl %ebp
	movl  %esp, %ebp
	pushl %esi
	pushl %ebx
	
	movl  12(%ebp), %ebx   # if (len == 0) return;
	testl %ebx, %ebx
	jz    done
	
	movl  8(%ebp), %esi  # data

next:	
	pushl 24(%ebp)
	pushl %esi
	call  *20(%ebp)
	addl  $8, %esp
	
	movl  %eax, 24(%ebp)
	
	addl  16(%ebp), %esi
	decl  %ebx 
	jnz   next
done:	

	movl  24(%ebp), %eax
	
	popl  %ebx
	popl  %esi
	popl  %ebp
	ret
