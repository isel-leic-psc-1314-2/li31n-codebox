#include <stdio.h>

void * find_any(void * data, unsigned len, unsigned dim,
               void * (*best)(void * ptr, void * ref), void * ref);
               
void * min_ptrs(void * ptr, void * ref) {
	int * xptr = (int *)ptr;
	int * xref = (int *)ref;
	return *xptr < *xref ? ptr : ref;
}
               
int min_of(int * items, unsigned len) {
	void * ret_ptr;
	int  * int_ret_ptr;
	int    min;

	int ref = 0x7fffffff;
	
	ret_ptr = find_any(items, len, sizeof (int), min_ptrs, &ref);
	
	int_ret_ptr = (int *)ret_ptr;
	min = *int_ret_ptr;
	return min;
	
	// Or, the short version:
	// return *(int*)ret_ptr;
}

int main() {
	int data[] = { 1, 43, -23, 0, -11, 51 };
	
	int min = min_of(data, 6);
	
	printf("min = %d\n", min);

	return 0;
}
