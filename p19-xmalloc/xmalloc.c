#include "xmalloc.h"
#include "dlist.h"

#define MEM_SIZE 64 * 1024

#define EXTRA_SIZE (sizeof (unsigned))
#define REAL_SIZE(SIZE) ((SIZE) + EXTRA_SIZE)
#define MIN_BLOCK_SIZE (sizeof (mblock_t))
#define PTR_ADJUST(TYPE, PTR, OFFSET) ((TYPE *)(((char *)(PTR)) + (OFFSET)))

unsigned char mem[MEM_SIZE];

dlist_t free_blocks;

typedef struct mblock {
	unsigned size;
	dnode_t  node;
} mblock_t;

int init;

void xmalloc_init() {
	mblock_t * pmem = (mblock_t *)mem;
	pmem->size = MEM_SIZE - EXTRA_SIZE;
	dlist_init(&free_blocks);
	dlist_insertFirst(&free_blocks, &(pmem->node));
}

void * xmalloc(unsigned size) {
	if (!init) {
		init = 1;
		xmalloc_init();
	}

	dnode_t * pnode;
	for(pnode = free_blocks.next; pnode != &free_blocks; pnode = pnode->next) {
		mblock_t * pblock = PTR_ADJUST(mblock_t, pnode, -EXTRA_SIZE);
		if (pblock->size >= size) {
			if (REAL_SIZE(size) <= REAL_SIZE(pblock->size) - MIN_BLOCK_SIZE) {
				mblock_t * pnewblock = PTR_ADJUST(mblock_t, pnode, pblock->size - REAL_SIZE(size));
				pnewblock->size = size;
				pblock->size -= REAL_SIZE(size);
				pnode = &(pnewblock->node);
			} else {
				dlist_remove(pnode);
			}
			return pnode;
		}
	}

	return 0;
}

void xfree(void * ptr) {
	dlist_insertFirst(&free_blocks, (dnode_t *)ptr);
}
