#include "dlist.h"

void dlist_insertAfter(dnode_t * refNode, dnode_t * newNode) {
	newNode->prev = refNode;
	newNode->next = refNode->next;
	
	newNode->prev->next = newNode;
	newNode->next->prev = newNode;
}

void dlist_insertBefore(dnode_t * refNode, dnode_t * newNode) {
	dlist_insertAfter(refNode->prev, newNode);
}

void dlist_insertFirst(dlist_t * list, dnode_t * newNode) {
	dlist_insertAfter(list, newNode);
}

void dlist_insertLast(dlist_t * list, dnode_t * newNode) {
	dlist_insertBefore(list, newNode);
}

bool dlist_isEmpty(dlist_t * list) {
	return list->next == list;
}

void dlist_init(dlist_t * list) {
	list->next = list->prev = list;
}

void dlist_remove(dnode_t * node) {
	node->prev->next = node->next;
	node->next->prev = node->prev;
}
