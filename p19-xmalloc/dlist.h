#include <stdbool.h>

struct dnode {
	struct dnode * prev;
	struct dnode * next;
};

typedef struct dnode dnode_t;
typedef struct dnode dlist_t;

void dlist_insertAfter(dnode_t * refNode, dnode_t * newNode);

void dlist_insertBefore(dnode_t * refNode, dnode_t * newNode);

void dlist_insertFirst(dlist_t * list, dnode_t * newNode);

void dlist_insertLast(dlist_t * list, dnode_t * newNode);

bool dlist_isEmpty(dlist_t * list);

void dlist_init(dlist_t * list);

void dlist_remove(dnode_t * node);
