#include <stdio.h>

int min_of(int * data, unsigned len);
int max_of(int * data, unsigned len);

int main() {

	int items[] = { 7, 0, -3, 11, -12, 1 };
	
	int min = min_of(items, 6);
	int max = max_of(items, 6);
	
	printf("min = %d | max = %d\n", min, max);

	return 0;
}
