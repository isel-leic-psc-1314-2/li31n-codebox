
	.text
	
	.globl max_of
	
max_of:
	pushl  %ebp
	movl   %esp, %ebp
	pushl  %esi
	
	movl   $0x80000000, %eax  # eax: máximo encontrado
	movl   12(%ebp), %ecx     # ecx: quantos faltam
	movl   8(%ebp), %esi      # esi: ponteiro para posição corrente

next:
	movl   (%esi), %edx       # edx: valor corrente
	cmpl   %eax, %edx
	jle    not_max
	movl   %edx, %eax
not_max:
	addl   $4, %esi
	decl   %ecx
	jnz    next

	popl   %esi
	popl   %ebp
	ret
