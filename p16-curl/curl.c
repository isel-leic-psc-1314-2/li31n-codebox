#include <stdio.h>
#include <curl/curl.h>

size_t process_data(void *buffer, size_t size, size_t nmemb, void *userp) {
	fprintf(stderr, "More data: %u bytes\n", size * nmemb);
	return fwrite(buffer, size, nmemb, userp);
}

void useCURL(CURL * curl, const char * url) {
	CURLcode res;
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, process_data);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, stdout);
	res = curl_easy_perform(curl);
	if (res) {
		fprintf(stderr, "error %u: %s\n", res, curl_easy_strerror(res));
	} else {
		long http_code = 0;
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
		fprintf(stderr, "http response code: %lu\n", http_code);
	}
}

int doWork(const char * url) {
   CURL * curl = curl_easy_init();
   if (curl) {
      useCURL(curl, url);
      curl_easy_cleanup(curl);
      return 0;
   } else {
      fprintf(stderr, "ERR: failed to init a CURL session the easy way\n");
      return 3;
   }
}

int main(int argc, const char * argv[]) {
   int ret;
   
   if (argc != 2) {
      fprintf(
		stderr,
		"use: %s SOME_URL\n"
		"where SOME_URL is a URL to get data from\n",
		argv[0]);
      return 1;
   }
   
   CURLcode code = curl_global_init(CURL_GLOBAL_DEFAULT);
   if (code == 0) {
      ret = doWork(argv[1]);
      curl_global_cleanup();
   } else {
      fprintf(stderr, "ERR: failed to globally init CURL\n");
      ret = 2;
   }
   return ret;
}
